from django.conf.urls import url
from . import views           # This line is new!


urlpatterns = [
    url(r'^$', views.index),     # This line has changed!
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^logout$', views.logout),
    url(r'^books$', views.books),
    url(r'^books/add$', views.add),
    url(r'^books/(?P<id>\d+)$', views.book_x),
    url(r'^users/(?P<id>\d+)$', views.user),
    url(r'^books/review/delete/(?P<id>\d+)$', views.delete),
    url(r'^add_review/(?P<id>\d+)$', views.add_review),
    url(r'^add_book_review/', views.add_book_review),
    url(r'^add', views.add)

]