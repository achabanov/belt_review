# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib import messages
from .models import *
import bcrypt



def index(request):
    return render(request, "first_app/index.html")


def user(request):
    if not "user_id" in request.sessions:
        return redirect('/')

    users = User.objects.all()
    context = {
        'user': users
    }
    return render(request, 'login/user.html', context)


def register(request):
    errors = []
    for key, val in request.POST.items():
        #print(key, val)
        if len(val) < 3:
            errors.append("{} must be at least two characters".format(key))
        #print(errors)

    if request.POST['password'] != request.POST['password_confirmation']:
        errors.append("Password and password confirmation don't match.")

    if errors:
        print("inside errors")
        for err in errors:
            messages.error(request, err)
        return redirect('/')

    else:
        try:
            print("inside try")
            User.objects.get(email=request.POST['email'])
            messages.error(request,"User with that email already exists.")
            print("User with that email already exists.")
            return redirect('/')
        except User.DoesNotExist:

            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            user = User.objects.create(name=request.POST['name'],
                                alias = request.POST['alias'],
                                password = hashpw,
                                email = request.POST['email'])
            request.session['user_id'] = user.id
            return redirect('/books')


def login(request):
    try:
        user = User.objects.get(email = request.POST['email'])
        # bcrypt.checkpw(given_password, stored_password)
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/books')
        else:
            messages.error(request, "Email/Password combination FAILED")
            print("Email/Password combination FAILED")
            return redirect('/')
    except User.DoesNotExist:
        messages.error(request, "Email does not exist. Please register")
        print("Email does not exist. Please register")
        return redirect('/')


def books(request):
    print("User id", request.session['user_id'])
    user = User.objects.get(id=request.session['user_id'])
    reviews = Review.objects.order_by("-created_at")[:3]
    books = Book.objects.filter(reviews__isnull=False)

    context = {
        'user': user,
        'reviews': reviews,
        'books': books
    }
    return render(request, "first_app/books.html", context)


def logout(request):
    request.session.clear()
    return redirect('/')


#######################################################

def add_book_review(request):
    if len(request.POST['new_author']) == 0:
        author = Author.objects.get(id=request.POST['author'])
    else:
        author = Author.objects.create(name=request.POST['new_author'])
    user = User.objects.get(id=request.session['user_id'])
    book = Book.objects.create(title=request.POST['title'], author=author, created_by=user)
    Review.objects.create(text=request.POST['text'], rating=request.POST['rating'], book=book, created_by=user)
    return redirect("/books/{}".format(book.id))


def add(request):
    all_authors = Author.objects.all()
    context = {
        'authors': all_authors
    }
    return render(request, 'first_app/add.html', context)
    pass


def delete(request, id):
    print("DELETING REVIEW")
    review = Review.objects.get(id=id)
    Review.objects.filter(id=id).delete()
    return redirect('/books/{}'.format(review.book_id))


def book_x(request, id):
    print("entered book x")
    book = Book.objects.get(id=id)
    reviews = Review.objects.filter(book=book)

    context = {
        'current_book': book,
        'book_reviews': reviews
    }
    return render(request, 'first_app/book_x.html', context)


def user(request, id):
    print("entered user x")
    user = User.objects.get(id=id)
    reviews = Review.objects.filter(created_by=user)
    count = reviews.count()

    context = {
        'user': user,
        'reviews': reviews,
        'count': count
    }
    return render(request, 'first_app/users.html', context)


def add_review(request, id):
    book = Book.objects.get(id=id)
    created_by = User.objects.get(id=request.session['user_id'])
    Review.objects.create(text=request.POST['text'], rating=request.POST['rating'], created_by=created_by, book=book)
    return redirect('/books/{}'.format(book.id))



def logout(request):
    print("LOGGING OUT")
    request.session.clear()
    return redirect('/')